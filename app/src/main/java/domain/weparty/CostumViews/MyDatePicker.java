package domain.weparty.CostumViews;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.DatePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


interface MyOnDateSetListener{
    void onDateSet(int year, int month, int day);
}

public class MyDatePicker extends android.support.v7.widget.AppCompatEditText implements MyOnDateSetListener {
    public MyDatePicker(Context context) {
        super(context);
        setInputType(0);
        setText(MyDatePickerDialog.getDateString());
    }

    public MyDatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        setInputType(0);
        setText(MyDatePickerDialog.getDateString());
    }

    public MyDatePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setInputType(0);
        setText(MyDatePickerDialog.getDateString());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            Log.d("TAG","event.getAction() == MotionEvent.ACTION_DOWN");
            MyDatePickerDialog tpd =  MyDatePickerDialog.newInstance(getId());
            tpd.show(((Activity)getContext()).getFragmentManager(),"TAG");
            return true;
        }
        return true;
    }

    @Override
    public void onDateSet(int year, int month, int day) {
        month++;
        setText("" + day + "/" + month + "/" + year);
    }

    public void setDate(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        setText(df.format(date));
    }

    public static class MyDatePickerDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        private static final String ARG_CONTAINER_EDIT_TEXT_VIEW = "edit_text_container";
        MyOnDateSetListener listener;

        public static MyDatePicker.MyDatePickerDialog newInstance(int tag) {
            MyDatePicker.MyDatePickerDialog datePickerDialog = new MyDatePicker.MyDatePickerDialog();
            Bundle args = new Bundle();
            args.putInt(ARG_CONTAINER_EDIT_TEXT_VIEW, tag);
            datePickerDialog.setArguments(args);
            return datePickerDialog;
        }

        public static String getDateString(){
            Date today = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(today);
            c.add(Calendar.DATE, 1);
            today = c.getTime();
            int month = today.getMonth() + 1;
            return("" + today.getDate() + "/" + month + "/" + 2017);
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            super.onCreateDialog(savedInstanceState);
            Date today = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(today);
            c.add(Calendar.DATE, 1);
            today = c.getTime();
            Dialog datePicker = new DatePickerDialog(getActivity(),this,2017,today.getMonth() ,today.getDate());

            if (getArguments() != null) {
                int tag = getArguments().getInt(ARG_CONTAINER_EDIT_TEXT_VIEW);
                listener = (MyOnDateSetListener) getActivity().findViewById(tag);
            }

            return datePicker;
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            Log.d("TAG", "dialog destroyed");
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            listener.onDateSet(year,month,dayOfMonth);
        }
    }
}



