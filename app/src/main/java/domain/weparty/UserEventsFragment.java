package domain.weparty;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import domain.weparty.Model.Event;
import domain.weparty.Model.User;
import domain.weparty.Utils.ImageHelper;
import domain.weparty.db.EventListener;


public class UserEventsFragment extends Fragment implements AdapterView.OnItemSelectedListener{
    ListView list;
    List<Event> data;
    UserEventListAdapter adapter;

    private UserEventListListener mListener;

    public void setDelegate(UserEventsFragment.UserEventListListener delegate){
        this.mListener = delegate;
    }

    public UserEventsFragment() {
        // Required empty public constructor
    }

    public static UserEventsFragment newInstance() {
        UserEventsFragment fragment = new UserEventsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View contentView = inflater.inflate(R.layout.fragment_event_list, container, false);
        final SpotsDialog dialog = new SpotsDialog(getActivity(), R.style.Custom);
        dialog.show();
        list = (ListView) contentView.findViewById(R.id.event_list_listView);
        ((MyApplication)getActivity().getApplication()).getAllEventsLocalAndObserve(new EventListener() {
            @Override
            public Integer addEvent(Event event) {
                return null;
            }

            @Override
            public void setUser(User user) {

            }

            @Override
            public void onAllEventsLoad(List<Event> events) {
                if(getActivity() != null) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    data = getUserEvents(events);
                    adapter = new UserEventsFragment.UserEventListAdapter();
                    list.setAdapter(adapter);

                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Event event = data.get(position);
                            if (mListener != null){
                                mListener.onItemClick(event);
                            }
                        }
                    });
                }
            }
        });

        return contentView;
    }

    private List<Event> getUserEvents(List<Event> events) {
        User currUser = MainActivity.getUser();
        List<Event> userEvents = new ArrayList<Event>();
        if (events != null) {
            for (int i = 0; i < events.size(); i++) {
                Event currEvent = events.get(i);
                if (currEvent.CreatorId.equals(currUser.Id)) {
                    userEvents.add(currEvent);
                } else if (currEvent.AttendingUsers != null) {
                    for (int j = 0; j < currEvent.AttendingUsers.size(); j++) {
                        User userToCheck = (User) currEvent.AttendingUsers.get(j);
                        if (userToCheck.Id.equals(currEvent)) {
                            userEvents.add(currEvent);
                        }
                    }
                }
            }
        }

        return userEvents;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EventsListFragment.EventListListener) {
            mListener = (UserEventsFragment.UserEventListListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement StudentListListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface UserEventListListener {
        void onItemClick(Event event);
    }

    class UserEventListAdapter extends BaseAdapter {
        LayoutInflater inflater = getActivity().getLayoutInflater();

        @Override
        public int getCount() {
            if(data != null) {
                return data.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null){
                convertView = inflater.inflate(R.layout.event_list_row,null);
            }

            TextView name = (TextView) convertView.findViewById(R.id.event_row_name);
            TextView description = (TextView) convertView.findViewById(R.id.event_row_description);
            final ImageView image = (ImageView)convertView.findViewById(R.id.event_row_image);
            TextView date = (TextView) convertView.findViewById(R.id.event_row_dateTv);
            TextView time = (TextView) convertView.findViewById(R.id.event_row_timeTv);


            final Event currEvent = data.get(position);
            name.setText(currEvent.Name);
            description.setText(currEvent.Description);
            date.setText(currEvent.getDateString());
            time.setText(currEvent.EventTime);

            image.setTag(currEvent.Image);

            if (currEvent.Image != null && !currEvent.Image.isEmpty() && !currEvent.Image.equals("")){
                ImageHelper.getImage(currEvent.Image, new ImageHelper.GetImageListener() {
                    @Override
                    public void onSuccess(Bitmap imagetoInsert) {
                        String tagUrl = image.getTag().toString();
                        if (tagUrl.equals(currEvent.Image)) {
                            image.setImageBitmap(imagetoInsert);
                            image.setScaleType(ImageView.ScaleType.FIT_XY);
                        }
                    }

                    @Override
                    public void onFail() {

                    }
                });
            }
            return convertView;
        }
    }
}
