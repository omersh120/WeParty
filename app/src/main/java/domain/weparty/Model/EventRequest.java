package domain.weparty.Model;

public class EventRequest {
    public String UserId;
    public String UserName;
    public String OwnerUserId;
    public String EventId;
    public String Status;

    public EventRequest(){}
}
