package domain.weparty.Model;


import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Event implements Serializable {
    public String Name;
    public String Description;
    public String Place;
    public Date EventDate;
    public String EventTime;
    public String CreatorName;
    public String CreatorId;
    public String Image;
    public long Timestamp;
    public String Id;
    public List<User> AttendingUsers;

    public Event(){}
    public Event(String name, String description, String place, Date date, String eventTime,
                 String creatorName, String creatorId, String image, User user){
        this.Name = name;
        this.Description = description;
        this.Place = place;
        this.EventDate = date;
        this.CreatorName = creatorName;
        this.CreatorId = creatorId;
        this.Image = image;
        this.Timestamp = System.currentTimeMillis();
        this.Id = java.util.UUID.randomUUID().toString().replace('-','1');
        this.EventTime = eventTime;
        this.AttendingUsers = new ArrayList<>();
        User newUser = new User();
        newUser.Name = user.Name;
        newUser.Id = user.Id;
        this.AttendingUsers.add(newUser);
    }

    public String getDateString(){
        if (this.EventDate == null){
            return "";
        }

        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return  format.format(this.EventDate);
    }

    public String getAttendingString(){
        String result = "";
        if (this.AttendingUsers != null && this.AttendingUsers.size() > 0){
            for (int i=0; i<this.AttendingUsers.size() - 1; i++) {
                result += this.AttendingUsers.get(i).Name + ", ";
            }
            result += this.AttendingUsers.get(this.AttendingUsers.size() - 1).Name;
        }

        return result;
    }
}
