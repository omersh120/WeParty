package domain.weparty.Model;


import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class User implements Serializable {
    public String Name;
    public String Address;
    public int Age;
    public String AboutMe;
    public Bitmap ProfileImage;
    public String Id;
    public HashMap<String,EventRequest> MyRequests;
    public HashMap<String,EventRequest> PendingRequest;

    public User(){}
    public User(String name, Bitmap image){
        this.Name = name;
        this.ProfileImage = image;
    }

    public List<EventRequest> getEventPendingRequests(String eventId) {
        List<EventRequest> requests = new ArrayList<>();
        if(this.PendingRequest!= null) {
            Object[] reqs = this.PendingRequest.values().toArray();
            if(reqs.length > 0) {
                for (int i = 0; i < reqs.length; i++) {
                    if(((EventRequest)reqs[i]).EventId.equals(eventId) &&
                            ((EventRequest)reqs[i]).Status.equals("0")){
                        requests.add((EventRequest) reqs[i]);
                    }
                }
            }
        }

        return requests;
    }

    public List<EventRequest> getEventRequests(String eventId) {
        List<EventRequest> requests = new ArrayList<>();
        if(this.MyRequests!= null) {
            Object[] reqs = this.MyRequests.values().toArray();
            if(reqs.length > 0) {
                for (int i = 0; i < reqs.length; i++) {
                    if(((EventRequest)reqs[i]).EventId.equals(eventId)){
                        requests.add((EventRequest) reqs[i]);
                    }
                }
            }
        }

        return requests;
    }


    public User(String id,String name,int age, String aboutMe, String address, Bitmap image){
        this.Name = name;
        this.Id = id;
        this.Address = address;
        this.AboutMe = aboutMe;
        this.Age = age;
        this.ProfileImage = image;
        MyRequests = new HashMap<>();
        PendingRequest = new HashMap<>();
    }
}
