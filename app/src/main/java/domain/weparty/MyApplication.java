package domain.weparty;

import android.app.Application;
import android.content.Context;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import java.util.List;

import domain.weparty.Model.Event;
import domain.weparty.Model.EventRequest;
import domain.weparty.Model.User;
import domain.weparty.db.EventDatabaseHandler;
import domain.weparty.db.EventListener;
import domain.weparty.db.FireBaseHandler;
import domain.weparty.db.ServerTime;

public class MyApplication extends Application  {

    EventDatabaseHandler mEventDatabaseHandler;
    FireBaseHandler mFireBaseHandler;
    private boolean isFirstTime = true;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        MyApplication.context = getApplicationContext();

        if (!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }
        long time;
        mEventDatabaseHandler = new EventDatabaseHandler(this);
        if(!mEventDatabaseHandler.checkIfConfigExists()) {
            time = 0;
        } else {
            time = Long.parseLong(mEventDatabaseHandler.getConfig());
        }
        mFireBaseHandler = new FireBaseHandler(mEventDatabaseHandler, time);
        ServerTime curTime = new ServerTime(mFireBaseHandler.mDatabase);
        curTime.getTime(new ServerTime.OnTimeRetrievedListener() {
            @Override
            public void onTimeRetrieved(Long timestamp) {
                mEventDatabaseHandler.insertLastUpdate(Long.toString(timestamp));
            }
        });
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }

    public void initializeData(User user, EventListener userListener)
    {
        userListener.setUser(user);
        mFireBaseHandler.initialUser(user);
        mFireBaseHandler.getUserByIdAndObserve(user.Id,userListener);
    }

    public void getUserByIdAndOberve(String userId, EventListener userListener) {
        mFireBaseHandler.getUserByIdAndObserve(userId,userListener);
    }

    public User getUser(User user) {
        User curUser = mEventDatabaseHandler.getUserById(user.Id);
        if(curUser != null) {
            curUser.ProfileImage = (user.ProfileImage);
            return curUser;
        }
        return user;
    }

    public void getAllEventsLocalAndObserve(final EventListener callback){
        //return StudentSql.getAllStudents(modelSql.getReadableDatabase());
        mFireBaseHandler.setSearchListener(callback);
        mEventDatabaseHandler.getAllEventsAndObserve(new EventListener() {
            @Override
            public Integer addEvent(Event event) {
                return null;
            }

            @Override
            public void setUser(User user) {

            }

            @Override
            public void onAllEventsLoad(List<Event> list) {
                if(list == null || list.size() == 0 || isFirstTime) {
                    getAllEventsFromRemoteAndObserve(callback, isFirstTime);
                    isFirstTime = false;
                } else {
                    callback.onAllEventsLoad(list);
                }
            }
        });
    }

    public void addEventRequest(EventRequest request) {
        mEventDatabaseHandler.insertEventRequest(request.UserId, request.OwnerUserId,
                            request.EventId, request.Status, request.UserName);
        mFireBaseHandler.addEventRequest(request);
        mFireBaseHandler.addEventPendingRequest(request);
    }

    public void updateEventRequest(EventRequest request) {
        mEventDatabaseHandler.updateEventRequest(request.UserId, request.OwnerUserId,
                request.EventId, request.Status, request.UserName);
        mFireBaseHandler.updateEventRequest(request);
        mFireBaseHandler.updatePendingEventRequest(request);
    }

    public void getAllEventsFromRemoteAndObserve(final EventListener callback , boolean isFirstTime){
        mFireBaseHandler.getAllEventsAndObserve(new EventListener() {
            @Override
            public Integer addEvent(Event event) {
                return null;
            }

            @Override
            public void setUser(User user) {

            }

            @Override
            public void onAllEventsLoad(List<Event> list) {
                callback.onAllEventsLoad(list);
            }
        }, isFirstTime);
        mFireBaseHandler.listenToNewEvents(callback);
    }

    public void updateUser(User user) {
        if (mEventDatabaseHandler.updateUser(user) > 0)
            mFireBaseHandler.updateUser(user);
    }

    public Event getEventById(String id) {
        return mEventDatabaseHandler.getEventById(id);
    }

    public void deleteEventById(String id) {
        mEventDatabaseHandler.deleteEventById(id);
        mFireBaseHandler.deleteEvent(id);
    }

    public void addEvent(Event event) {
        if (mEventDatabaseHandler.addEvent(event) > 0)
            mFireBaseHandler.addEvent(event);
    }

    public void updateEvent(Event event){
        if (mEventDatabaseHandler.updateEvent(event) > 0)
            mFireBaseHandler.addEvent(event);
    }
}
