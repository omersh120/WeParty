package domain.weparty;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import dmax.dialog.SpotsDialog;
import domain.weparty.CostumViews.MyDatePicker;
import domain.weparty.CostumViews.MyTimePicker;
import domain.weparty.Model.Event;
import domain.weparty.Model.User;
import domain.weparty.Utils.ImageHelper;
import domain.weparty.Utils.PlaceJSONParser;

import static android.app.Activity.RESULT_OK;

public class CreateEventFragment extends Fragment {
    private OnCreateEventListener mListener;
    public Bitmap currBitmap = null;
    String currBitmapUrl;
    AutoCompleteTextView atvPlaces;
    PlacesTask placesTask;
    ParserTask parserTask;
    ImageView image;
    TextView imageTv;
    Event eventForEdit;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    SpotsDialog dialog;

    void setDelegate(OnCreateEventListener delegate){
        this.mListener = delegate;
    }

    public CreateEventFragment() {
        // Required empty public constructor
    }

    public static CreateEventFragment newInstance(Event eventForEdit) {
        CreateEventFragment fragment = new CreateEventFragment();
        fragment.eventForEdit = eventForEdit;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_create_event, container, false);
        if (eventForEdit != null){
            setEvent(view, eventForEdit);
        }
        dialog = new SpotsDialog(getActivity(), R.style.Custom);
        imageTv = (TextView)view.findViewById(R.id.create_event_uploadImageTV);
        saveButtonEventHandle(view);
        searchPlaceHandle(view);
        uploadImageHandle(view);

        Log.d("TAG", "create event create view");
        return view;
    }


    private void setEvent(View view, Event event){
        EditText nameET = (EditText) view.findViewById(R.id.create_event_nameET);
        nameET.setText(event.Name);
        EditText descriptionET = (EditText) view.findViewById(R.id.create_event_descriptionET);
        descriptionET.setText(event.Description);
        TextView placeET = (TextView) view.findViewById(R.id.create_event_atv_places);
        placeET.setText(event.Place);
        Button saveBtn = (Button)view.findViewById(R.id.create_event_saveBTN);
        saveBtn.setText("SAVE");
        
        if (event.EventDate != null){
            MyTimePicker timeTV = (MyTimePicker) view.findViewById(R.id.create_event_pickTimeTV);
            timeTV.setTime(event.EventTime);
            MyDatePicker dateTV = (MyDatePicker) view.findViewById(R.id.create_event_pickDateTV);
            dateTV.setDate(event.EventDate);
        }

        final ImageView imageView = (ImageView)view.findViewById(R.id.create_event_imageView);
        final TextView imageTv = (TextView)view.findViewById(R.id.create_event_uploadImageTV);
        if(event.Image != null) {
            ImageHelper.getImage(event.Image, new ImageHelper.GetImageListener() {
                @Override
                public void onSuccess(Bitmap image) {
                    currBitmap = image;
                    imageView.setImageBitmap(image);
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    imageTv.setVisibility(View.INVISIBLE);
                }
                @Override
                public void onFail() {
                    imageTv.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void uploadImageHandle(View view) {
        this.image= (ImageView) view.findViewById(R.id.create_event_imageView);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
    }

    private void searchPlaceHandle(View view) {
        atvPlaces = (AutoCompleteTextView) view.findViewById(R.id.create_event_atv_places);
        atvPlaces.setThreshold(1);

        atvPlaces.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    private void saveButtonEventHandle(final View container) {
        Button saveButton = (Button)container.findViewById(R.id.create_event_saveBTN);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText nameET = (EditText) container.findViewById(R.id.create_event_nameET);
                final EditText descriptionET = (EditText) container.findViewById(R.id.create_event_descriptionET);
                final TextView placeET = (TextView) container.findViewById(R.id.create_event_atv_places);
                final MyTimePicker timeTV = (MyTimePicker) container.findViewById(R.id.create_event_pickTimeTV);
                final Date currDate = getDate(container);
                final User currUser = MainActivity.getUser();
                final Event[] newEvent = new Event[1];
                if (validateEvent(nameET.getText().toString(), descriptionET.getText().toString(),
                        placeET.getText().toString(), currDate, timeTV.getText().toString(),currBitmap)) {
                    dialog.show();

                    String imageName = java.util.UUID.randomUUID().toString().replace('-', '1');
                    ImageHelper.saveImage(currBitmap, imageName + ".jpeg", new ImageHelper.SaveImageListener() {
                        @Override
                        public void complete(String url) {
                            currBitmapUrl = url;

                            newEvent[0] = new Event(nameET.getText().toString(), descriptionET.getText().toString(),
                                    placeET.getText().toString(), currDate, timeTV.getText().toString(), currUser.Name,
                                    currUser.Id, currBitmapUrl, ((MainActivity)getActivity()).getUser());
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            saveOrUpdateEvent(newEvent[0]);
                        }

                        @Override
                        public void fail() {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            Toast toast = Toast.makeText(getActivity(), "Oops! failed to save image", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });
                }
            }
        });
    }


    private void saveOrUpdateEvent(Event newEvent) {
        try {
            if (eventForEdit != null) {
                newEvent.Id = eventForEdit.Id;
                newEvent.AttendingUsers = eventForEdit.AttendingUsers;
                ((MyApplication) getActivity().getApplication()).updateEvent(newEvent);
            } else {
                ((MyApplication) getActivity().getApplication()).addEvent(newEvent);
            }
            if (mListener != null) {
                mListener.onCreateOrUpdateEvent(eventForEdit == null);
            }
        } catch (Exception e) {
            Log.d("Add Event:", "failed to add event", e);
            Toast toast = Toast.makeText(getActivity(), "Oops! failed to save event", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    public  boolean validateEvent(String name, String desc, String place , Date date,
                                 String time, Bitmap image) {
        boolean isValid = true;
        if(name.equals("")) {
            Toast toast = Toast.makeText(getActivity(), "Oops! you have to fill event name", Toast.LENGTH_SHORT);
            toast.show();
            isValid = false;
        }
        else if(image == null){
            Toast toast = Toast.makeText(getActivity(), "Oops! you have to put image", Toast.LENGTH_SHORT);
            toast.show();
            isValid = false;
        }
        else if(place.equals("")){
            Toast toast = Toast.makeText(getActivity(), "Oops! you have to fill place", Toast.LENGTH_SHORT);
            toast.show();
            isValid = false;
        }
        else if(desc.equals("")){
            Toast toast = Toast.makeText(getActivity(), "Oops! you have to fill description", Toast.LENGTH_SHORT);
            toast.show();
            isValid = false;
        }
        else if(time.equals("")){
            Toast toast = Toast.makeText(getActivity(), "Oops! something wrong with your time", Toast.LENGTH_SHORT);
            toast.show();
            isValid = false;
        }
        else if(date.before(new Date())){
            Toast toast = Toast.makeText(getActivity(), "Oops! you have to set future event date", Toast.LENGTH_SHORT);
            toast.show();
            isValid = false;
        }
        return isValid;
    }

    public Date getDate(View container) {
        MyDatePicker dateTV = (MyDatePicker) container.findViewById(R.id.create_event_pickDateTV);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date date;
        try {
            date = format.parse(dateTV.getText().toString());
        } catch (ParseException e) {
            date = new Date();
        }

        return date;
    }

    private void dispatchTakePictureIntent() {
        startActivityForResult(new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                REQUEST_IMAGE_CAPTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();

            try {
                currBitmap = MediaStore.Images.Media.getBitmap(MyApplication.getAppContext().getContentResolver(), uri);
                currBitmap = ImageHelper.getResizedBitmap(currBitmap, 500);
                image.setImageBitmap(currBitmap);
                image.setScaleType(ImageView.ScaleType.FIT_XY);
                imageTv.setVisibility(View.INVISIBLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnCreateEventListener) context;
        } catch (ClassCastException castException) {
        }
    }


    public interface OnCreateEventListener {
        void onCreateOrUpdateEvent(boolean isCreate);
    }

    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            String data = "";
            String key = "key=" + getString(R.string.google_maps_key);
            String input="";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            String types = "types=geocode";
            String sensor = "sensor=false";
            String parameters = input+"&"+types+"&"+sensor+"&"+key;

            String output = "json";
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;

            try{
                data = ImageHelper.downloadUrl(url);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[] { "description"};
            int[] to = new int[] { android.R.id.text1 };

            //Fragment mtFragment = getFragmentManager().findFragmentById(R.id.tab_create_event);
            if(getFragmentManager() != null) {
                // Creating a SimpleAdapter for the AutoCompleteTextView
                SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

                // Setting the adapter
                atvPlaces.setAdapter(adapter);
            }
        }
    }
}
