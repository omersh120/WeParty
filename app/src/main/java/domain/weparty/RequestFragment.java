package domain.weparty;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import domain.weparty.Model.Event;
import domain.weparty.Model.EventRequest;
import domain.weparty.Model.User;

public class RequestFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final String ARG_ITEM = "item";
    private Event currEvent;
    List<EventRequest> data;
    RequestFragment.EventListAdapter adapter;
    ListView list;

    private RequestListListener mListener;

    public interface RequestListListener {
        void onItemClick(EventRequest event);
    }

    public RequestFragment() {
        // Required empty public constructor
    }

    public static RequestFragment newInstance(Event item) {
        RequestFragment fragment = new RequestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item.Id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String eventId = getArguments().getString(ARG_ITEM);
        currEvent = ((MyApplication)getActivity().getApplication()).getEventById(eventId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment_request, container, false);

        list = (ListView) contentView.findViewById(R.id.event_list_listView);
        List<EventRequest> requests = ((MyApplication)getActivity().getApplication()).getUser
                (((MainActivity) getActivity()).getUser()).
                getEventPendingRequests(currEvent.Id);
        if (getActivity() != null) {
            data = requests;
            adapter = new RequestFragment.EventListAdapter(getActivity());
            list.setAdapter(adapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    EventRequest event = data.get(position);
                    if (mListener != null) {
                        mListener.onItemClick(event);
                    }
                }
            });
        }
        return contentView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class EventListAdapter extends BaseAdapter {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        private Context mContext;

        public EventListAdapter(Context context){
            mContext = context;
        }

        public EventListAdapter(){
        }

        @Override
        public int getCount() {
            if(data != null) {
                return data.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null){
                convertView = inflater.inflate(R.layout.request_list_row,null);
            }

            final TextView requestName = (TextView) convertView.findViewById(R.id.event_row_request_name);
            final TextView requestText = (TextView) convertView.findViewById(R.id.event_row_request_text);
            final Button approveBtn = (Button) convertView.findViewById(R.id.approveBtn);
            final Button denyBtn = (Button) convertView.findViewById(R.id.denyBtn);
            final EventRequest event = data.get(position);

            requestName.setText(event.UserName);
            requestName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mContext != null && mContext instanceof MainActivity){
                        ((MainActivity)mContext).moveToUserProfile(event.UserId);
                    }
                }
            });

            requestText.setText(" has requested to join");
            denyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventRequest req = new EventRequest();
                    req.OwnerUserId = event.OwnerUserId;
                    req.UserName = event.UserName;
                    req.EventId = event.EventId;
                    req.UserId = event.UserId;
                    req.Status = "2";
                    ((MyApplication)getActivity().getApplication()).updateEventRequest(req);
                    approveBtn.setVisibility(View.INVISIBLE);
                    denyBtn.setVisibility(View.INVISIBLE);
                    requestText.setText(" denied to " + currEvent.Name);
                }
            });
            approveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventRequest req = new EventRequest();
                    req.OwnerUserId = event.OwnerUserId;
                    req.UserName = event.UserName;
                    req.EventId = event.EventId;
                    req.UserId = event.UserId;
                    req.Status = "1";
                    User user = new User();
                    user.Id = req.UserId;
                    user.Name = req.UserName;
                    if(currEvent.AttendingUsers == null) {
                        currEvent.AttendingUsers = new ArrayList<User>();
                    }

                    currEvent.AttendingUsers.add(user);
                    ((MyApplication)getActivity().getApplication()).updateEvent(currEvent);
                    ((MyApplication)getActivity().getApplication()).updateEventRequest(req);

                    approveBtn.setVisibility(View.INVISIBLE);
                    denyBtn.setVisibility(View.INVISIBLE);
                    requestText.setText(" approved to " + currEvent.Name);
                }
            });
            return convertView;
        }
    }
}
