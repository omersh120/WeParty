package domain.weparty;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MenuItem;

import com.jetradar.multibackstack.BackStackActivity;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import domain.weparty.Model.Event;
import domain.weparty.Model.User;
import domain.weparty.db.EventListener;

public class MainActivity extends BackStackActivity implements  EventsListFragment.EventListListener,
        CreateEventFragment.OnCreateEventListener, EventListener,
                 EventItemFragment.OnEventItemListener, UserEventsFragment.UserEventListListener, FragmentManager.OnBackStackChangedListener {
    Map<Integer, Fragment> fragmentsMap = new HashMap<Integer, Fragment>();
    static final Integer nullTabId = -999;
    static Integer currTab = nullTabId;
    int searchTabPosition = 0;
    OnTabSelectListener listener;
    BottomBar bottomBar;
    static User user;
    Map<Integer, Integer> tabsLocationMap = new HashMap<Integer, Integer>();
    static final int REQUEST_WRITE_STORAGE = 11;

    public BottomBar getBottomBar() {
        return bottomBar;
    }

    public Map<Integer, Fragment> getFragmentMap() {
        return fragmentsMap;
    }

    public static User getUser() {
        if(user == null) {
            user = new User();
        }

        return  user;
    }

    public void setFacebookDetails(User user) {
        getUser().ProfileImage = (user.ProfileImage);
        getUser().Name = (user.Name);
    }

    @Override
    public Integer addEvent(Event event) {
        return null;
    }

    @Override
    public void setUser(User newUser) {
        getUser().AboutMe = (newUser.AboutMe);
        getUser().Address = (newUser.Address);
        getUser().Age = (newUser.Age);
    }

    @Override
    public void onAllEventsLoad(List<Event> list) {

    }

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_main);
        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        initFragements();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        shouldDisplayHomeUp();

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new FacebookFragment();
            fm.beginTransaction()
                    .add(R.id.contentContainer, fragment)
                    .commit();
        }

        handleWriteExternalStoragePermission();
    }

    private void handleWriteExternalStoragePermission() {
        boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this,new String[]{
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }
    }

    public void HandleNavigationButton() {
        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        listener = new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                    FragmentManager fm = getSupportFragmentManager();
                    for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                        fm.popBackStack();
                    }
                    Fragment newFragment = fragmentsMap.get(tabId);
                    if (newFragment != null){
                        replaceFragment(newFragment);
                        currTab = tabId;
                    }
            }
        };
        bottomBar.setOnTabSelectListener(listener);
    }

    private  void initFragements(){
        Integer tabLocation = 0;
        EventsListFragment eventListFragment = new EventsListFragment().newInstance();
        eventListFragment.setDelegate(this);
        fragmentsMap.put(R.id.tab_event_list, eventListFragment);
        tabsLocationMap.put(R.id.tab_event_list, tabLocation);
        tabLocation++;

        CreateEventFragment createEventFragment = new CreateEventFragment().newInstance(null);
        createEventFragment.setDelegate(this);
        fragmentsMap.put(R.id.tab_create_event, createEventFragment);
        tabLocation++;

        UserEventsFragment userEventFragment = new UserEventsFragment().newInstance();
        userEventFragment.setDelegate(this);
        fragmentsMap.put(R.id.tab_user_events, userEventFragment);
        tabsLocationMap.put(R.id.tab_user_events, tabLocation);
        tabLocation++;

        UserProfileFragment userProfileFragment = new UserProfileFragment().newInstance(null);
        fragmentsMap.put(R.id.tab_user_profile, userProfileFragment);
        tabsLocationMap.put(R.id.tab_user_profile, tabLocation);
    }

    @Override
    public void onItemClick(Event event) {
        EventItemFragment fragment = new EventItemFragment().newInstance(event);
        fragment.setDelegate(this);
        replaceFragmentWithBack(fragment);
    }

    public void onRequestClick(Event event) {
        RequestFragment fragment = new RequestFragment().newInstance(event);
        replaceFragmentWithBack(fragment);
    }


    private void replaceFragment(Fragment fragment) {
        try{
            android.support.v4.app.FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
            tran.replace(R.id.contentContainer,fragment, "TAG");
            tran.commit();
        }
        catch(Exception ex){
            Log.d("TAG", ex.toString());
        }
    }

    private void replaceFragmentWithBack(Fragment fragment) {
        try{
            if (fragment.isAdded()){
                return;
            }
            android.support.v4.app.FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
            tran.addToBackStack(null);
            tran.replace(R.id.contentContainer,fragment, "TAG");
            tran.commit();
        }
        catch(Exception ex){
            Log.d("TAG", ex.toString());
        }
    }

    @Override
    public void onCreateOrUpdateEvent(boolean isCreate) {
        CreateEventFragment createEventFragment = new CreateEventFragment().newInstance(null);
        createEventFragment.setDelegate(this);
        fragmentsMap.put(R.id.tab_create_event, createEventFragment);

        if (!isCreate){
            Fragment newFragment = fragmentsMap.get(R.id.tab_event_list);
            if (newFragment != null){
                replaceFragmentWithBack(newFragment);
            }
        }
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        bottomBar.selectTabWithId(R.id.tab_user_events);
    }

    @Override
    public void onDeleteEvent(String eventId) {
        bottomBar.selectTabWithId(R.id.tab_user_profile);
    }

    @Override
    public void onEditEvent(Event event) {
        CreateEventFragment fragment = new CreateEventFragment().newInstance(event);
        fragment.setDelegate(this);
        replaceFragmentWithBack(fragment);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp(){
        boolean canback = getSupportFragmentManager().getBackStackEntryCount()>0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try{
            switch (item.getItemId()) {
                case android.R.id.home:
                    onBackPressed();
            }
            return super.onOptionsItemSelected(item);
        }
        catch (Exception ex){
         return true;
        }
    }

    public void moveToUserProfile(String userId) {
        String userIdToSend;
        if (userId == null || userId == ""){
            userIdToSend = user.Id;
        }
        else
        {
            userIdToSend = userId;
        }

        UserProfileFragment fragment = new UserProfileFragment().newInstance(userIdToSend);
        replaceFragmentWithBack(fragment);

        Log.d("TAG", "moveToUserProfile");
    }
}