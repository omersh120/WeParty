package domain.weparty;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.List;

import domain.weparty.Model.Event;
import domain.weparty.Model.EventRequest;
import domain.weparty.Model.User;
import domain.weparty.Utils.ImageHelper;

public class EventItemFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_ITEM = "item";

    private Event currEvent;
    private EventItemFragment.OnEventItemListener mListener;
    private ExpandableLayout expandable_layout_attending;
    private boolean isAttendingExpand= false;
    private  Drawable expandMoreImage;
    private  Drawable expandLessImage;
    void setDelegate(EventItemFragment.OnEventItemListener delegate){
        this.mListener = delegate;
    }

    public EventItemFragment() {}

    public static EventItemFragment newInstance(Event item) {
        EventItemFragment fragment = new EventItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, item.Id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_item, container, false);
        String eventId = getArguments().getString(ARG_ITEM);
        currEvent = ((MyApplication)getActivity().getApplication()).getEventById(eventId);
        Button requestBtn = (Button)view.findViewById(R.id.event_item_requestBtn);
        TextView attendindLabel = (TextView)view.findViewById(R.id.expand_button_attending);
        attendindLabel.setText("Attending(" + currEvent.AttendingUsers.size() + ")");

        User currUser= MainActivity.getUser();
        currUser = ((MyApplication)getActivity().getApplication()).getUser(currUser);
        List<EventRequest> req = currUser.getEventRequests(currEvent.Id);
        if(req.size() == 0) {
            changeButton(requestBtn, "JOIN", true, R.color.colorGreen);
        } else if(req.get(0).Status.equals("0")) {
            changeButton(requestBtn, "REQUESTED", false, R.color.colorGrey);
        } else if(req.get(0).Status.equals("1")) {
            changeButton(requestBtn, "ATTENDING", false, R.color.colorBlue);
        }else if(req.get(0).Status.equals("2")) {
            changeButton(requestBtn, "REQUEST DENIED", false, R.color.colorRed);
        }

        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyApplication) getActivity().getApplication()).updateEvent(currEvent);
            }
        });

        handleExpanding(view);
        return view;
    }

    private void changeButton(Button requestBtn, String text, boolean enabled, int color) {
        requestBtn.setText(text);
        requestBtn.setEnabled(enabled);
        requestBtn.setBackgroundColor(getResources().getColor(color));
    }

    private void handleExpanding(View view) {
        expandable_layout_attending = (ExpandableLayout) view.findViewById(R.id.expandable_layout_attending);
        expandable_layout_attending.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
            @Override
            public void onExpansionUpdate(float expansionFraction, int state) {}
        });
        view.findViewById(R.id.expand_button_attending).setOnClickListener(this);

        User currUser= MainActivity.getUser();
        currUser = ((MyApplication)getActivity().getApplication()).getUser(currUser);
        TextView reqText = (TextView)view.findViewById(R.id.event_requests);
        Button requestBtn = (Button)view.findViewById(R.id.event_item_requestBtn);
        if (!currEvent.CreatorId.equals(currUser.Id)) {
            reqText.setVisibility(View.INVISIBLE);
        }
        else{
            requestBtn.setVisibility(View.INVISIBLE);
            final int requestCount = currUser.getEventPendingRequests(currEvent.Id).size();
            reqText.setText("Requests(" + currUser.getEventPendingRequests(currEvent.Id).size() + ")");
            reqText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(requestCount > 0) {
                        ((MainActivity)getActivity()).onRequestClick(currEvent);
                    }
                }
            });
        }

        initExpandImages();
    }

    private void initExpandImages() {
        expandMoreImage = getContext().getResources().getDrawable( R.drawable.ic_expand_more_24dp );
        expandMoreImage.setBounds( 0, 0, 60, 60 );
        expandLessImage = getContext().getResources().getDrawable( R.drawable.ic_expand_less_24dp );
        expandMoreImage.setBounds( 0, 0, 60, 60 );
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.expand_button_attending) {
            clickOnAttendingList(view);
        }
    }

    private void clickOnAttendingList(View view) {
        if (isAttendingExpand) {
            isAttendingExpand = false;
            TextView tv = (TextView)view.findViewById(R.id.expand_button_attending);
            tv.setCompoundDrawablesWithIntrinsicBounds( expandMoreImage, null, null, null);
            expandable_layout_attending.collapse();

        } else {
            isAttendingExpand = true;
            TextView tv = (TextView)view.findViewById(R.id.expand_button_attending);
            tv.setCompoundDrawablesWithIntrinsicBounds( expandLessImage, null, null, null);
            expandable_layout_attending.expand();
        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        TextView nameTv = (TextView) view.findViewById(R.id.event_item_name);
        nameTv.setText(currEvent.Name);

        TextView placeTv = (TextView) view.findViewById(R.id.event_item_place);
        placeTv.setText(currEvent.Place);

        TextView creatorNameTv = (TextView) view.findViewById(R.id.event_item_Creator_name);
        creatorNameTv.setText(currEvent.CreatorName);

        TextView dateTv = (TextView) view.findViewById(R.id.event_item_date);
        dateTv.setText(currEvent.getDateString());

        TextView timeTv = (TextView) view.findViewById(R.id.event_item_time);
        timeTv.setText(currEvent.EventTime);

        TextView descriptionTv = (TextView) view.findViewById(R.id.event_item_description);
        descriptionTv.setText(currEvent.Description);

        TextView creatorTv = (TextView) view.findViewById(R.id.event_item_Creator_name);
        creatorTv.setText(currEvent.CreatorName);
        creatorTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context mContext = getContext();
                if (mContext != null && mContext instanceof MainActivity){
                    ((MainActivity)mContext).moveToUserProfile(currEvent.CreatorId);
                }
            }
        });

        TextView attendingTv = (TextView) view.findViewById(R.id.event_item_attending);
        attendingTv.setText(currEvent.getAttendingString());

        handleImage(view);
    }

    private void handleImage(View view) {
        final ImageView imageView = (ImageView) view.findViewById(R.id.event_item_image);
        if(currEvent.Image != null) {
            ImageHelper.getImage(currEvent.Image, new ImageHelper.GetImageListener() {
                @Override
                public void onSuccess(Bitmap image) {
                    imageView.setImageBitmap(image);
                }
                @Override
                public void onFail() {
                    Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(),R.drawable.party_default);
                    imageView.setImageBitmap(icon);
                }
            });
        } else {
            Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(),R.drawable.party_default);
            imageView.setImageBitmap(icon);
        }

        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        final Button requestBtn = (Button) view.findViewById(R.id.event_item_requestBtn);
        requestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventRequest request = new EventRequest();
                request.UserId = ((MainActivity)getActivity()).getUser().Id;
                request.UserName = ((MainActivity)getActivity()).getUser().Name;
                request.EventId = currEvent.Id;
                request.OwnerUserId = currEvent.CreatorId;
                request.Status = "0";
                ((MyApplication)getActivity().getApplication()).addEventRequest(request);
                changeButton(requestBtn, "Requested", false, R.color.colorGrey);
            }
        });
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        User currUser= MainActivity.getUser();
        if (currEvent.CreatorId.equals(currUser.Id)) {
            inflater.inflate(R.menu.event_item_menu, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                if (mListener != null){
                    mListener.onEditEvent(currEvent);
                }
                Log.d("TAG", "edit button pressed");
                return true;
            case R.id.menu_delete:
                ((MyApplication)getActivity().getApplication()).deleteEventById(currEvent.Id);
                if (mListener != null){
                    mListener.onDeleteEvent(currEvent.Id);
                }
                Log.d("TAG", "delete button pressed");
                return false;
            default:
                break;
        }
        return true;
    }

    public interface OnEventItemListener {
        void onDeleteEvent(String eventId);
        void onEditEvent(Event event);
    }
}