package domain.weparty.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import domain.weparty.Model.Event;
import domain.weparty.Model.EventRequest;
import domain.weparty.Model.User;
import domain.weparty.Utils.ImageHelper;

import static java.lang.Integer.parseInt;

public class EventDatabaseHandler extends SQLiteOpenHelper {
    private static final String DB_NAME = "EventDatabase.db";
    private static final String EVENT_TABLE_NAME = "event_table";
    private static final String EVENT_REQUEST_TABLE_NAME = "event_request";
    private static final String USER_TABLE_NAME = "user_table";
    private static final String EVENT_ATTENDANT_TABLE_NAME = "event_users_table";
    private static final String KEY_ID = "_id";
    private static final String KEY_NAME = "_name";
    private static final String KEY_DESCRIPTION = "_description";
    private static final String KEY_DATE = "_date";
    private static final String KEY_TIME = "_time";
    private static final String KEY_LOCATION = "_location";
    private static final String KEY_CREATOR = "_creator";
    private static final String KEY_AGE = "_age";
    private static final String KEY_ADDRESS = "_address";
    private static final String KEY_ABOUT = "_about";
    private static final String KEY_CREATOR_ID = "_creator_id";
    private static final String KEY_EVENT_IMAGE = "_Image";
    private static final String KEY_IMAGE = "_profileImage";
    private static final String KEY_PLACE = "_place";
    private EventListener eventListener;
    String CREATE_EVENTS_TABLE = "CREATE TABLE "+EVENT_TABLE_NAME+" ("+KEY_ID +" TEXT PRIMARY KEY,"
            +KEY_NAME+" TEXT,"+KEY_DESCRIPTION+" TEXT,"+KEY_DATE+" TEXT,"
            +KEY_LOCATION+" TEXT,"
            +KEY_CREATOR+" TEXT, " +KEY_CREATOR_ID+" TEXT, "  +KEY_EVENT_IMAGE+ " TEXT, "
            + KEY_PLACE +" TEXT, " + KEY_TIME + " TEXT)";

    String CREATE_USER_TABLE = "CREATE TABLE "+USER_TABLE_NAME+" ("+KEY_ID +" TEXT PRIMARY KEY,"
            +KEY_NAME+" TEXT,"+ KEY_AGE+" TEXT,"+ KEY_ADDRESS+" TEXT,"+KEY_ABOUT+" TEXT,"+ KEY_IMAGE+ " BLOB)";

    String CREATE_CONFIG_TABLE = "CREATE TABLE CONFIG(updated TEXT)";
    String CREATE_EVENT_REQUEST_TABLE = "CREATE TABLE "+ EVENT_REQUEST_TABLE_NAME+"(user_id TEXT, owner_user TEXT, event_id TEXT, status TEXT, user_name TEXT)";
    String CREATE_EVENT_USERS_TABLE = "CREATE TABLE "+EVENT_ATTENDANT_TABLE_NAME+" (event_id TEXT, user_id TEXT )";

    String DROP_CONFIG_TABLE = "DROP TABLE IF EXISTS CONFIG";
    String DROP_EVENT_USERS_TABLE = "DROP TABLE IF EXISTS " +  EVENT_REQUEST_TABLE_NAME;
    String DROP_EVENT_TABLE = "DROP TABLE IF EXISTS "+EVENT_TABLE_NAME;
    String DROP_EVENT_ATTENDANT_TABLE = "DROP TABLE IF EXISTS "+EVENT_ATTENDANT_TABLE_NAME;
    String DROP_USER_TABLE = "DROP TABLE IF EXISTS "+USER_TABLE_NAME;

    public EventDatabaseHandler(Context context) {
        super(context, DB_NAME, null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DROP_EVENT_TABLE);
        db.execSQL(DROP_USER_TABLE);
        db.execSQL(DROP_EVENT_ATTENDANT_TABLE);
        db.execSQL(DROP_CONFIG_TABLE);
        db.execSQL(DROP_EVENT_USERS_TABLE);
        db.execSQL(CREATE_CONFIG_TABLE);
        db.execSQL(CREATE_EVENT_REQUEST_TABLE);
        db.execSQL(CREATE_EVENTS_TABLE);
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_EVENT_USERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void insertLastUpdate(String lastUpdated) {
        SQLiteDatabase db = this.getReadableDatabase();

        try{
            String deleteSql = "DELETE FROM CONFIG";
            String sql = "INSERT INTO CONFIG (updated) VALUES(  '" + lastUpdated +"\')";
            db.execSQL(deleteSql);
            db.execSQL(sql);
        } catch(Exception ex) {
            Log.e("error while insert config",ex+"");
        }
    }

    public String getConfig() {
        String lastUpdate = null;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM CONFIG";
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    lastUpdate = (cursor.getString(0));
                }
            }
            db.close();
        }
        catch (Exception e){
            Log.e("error",e+"");
        }
        return lastUpdate;
    }

    public HashMap<String,EventRequest> getUserRequests(String userId) {
        HashMap<String,EventRequest> requests = new HashMap<String,EventRequest>();
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT user_id,owner_user,event_id,status,user_name FROM "+ EVENT_REQUEST_TABLE_NAME+
                    " WHERE user_id = \'" + userId +"\'";

            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    EventRequest newRequest = new EventRequest();
                    newRequest.UserId = (cursor.getString(0));
                    newRequest.OwnerUserId = (cursor.getString(1));
                    newRequest.EventId = (cursor.getString(2));
                    newRequest.Status = (cursor.getString(3));
                    newRequest.UserName = (cursor.getString(4));
                    requests.put(newRequest.EventId + newRequest.UserId,newRequest);
                }
            }
            db.close();
        }
        catch (Exception e){
            Log.e("error",e+"");
        }
        return requests;
    }

    public HashMap<String,EventRequest> getUserPendingRequests(String userId) {
        HashMap<String,EventRequest> requests = new HashMap<String,EventRequest>();
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT user_id,owner_user,event_id,status, user_name FROM "+ EVENT_REQUEST_TABLE_NAME+
                    " WHERE owner_user = \'" + userId +"\'";

            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    EventRequest newRequest = new EventRequest();
                    newRequest.UserId = (cursor.getString(0));
                    newRequest.OwnerUserId = (cursor.getString(1));
                    newRequest.EventId = (cursor.getString(2));
                    newRequest.Status = (cursor.getString(3));
                    newRequest.UserName = (cursor.getString(4));
                    requests.put(newRequest.EventId + newRequest.UserId, newRequest);
                }
            }
            db.close();
        }
        catch (Exception e){
            Log.e("error",e+"");
        }
        return requests;
    }

    public void updateEventRequest(String userId, String ownerUser, String eventId, String status, String userName) {
        SQLiteDatabase db = this.getReadableDatabase();

        try{
            String sql = "UPDATE " + EVENT_REQUEST_TABLE_NAME +
                    " SET status = " + status + " WHERE user_id = \'"
                    + userId + "\' AND owner_user = \'" + ownerUser
                    + "\' AND event_id = \'" + eventId +  "\'";
            db.execSQL(sql);
        } catch(Exception ex) {
            Log.e("error while update request",ex+"");
        }
    }

    public void insertEventRequest(String userId, String ownerUser, String eventId, String status, String userName) {
        SQLiteDatabase db = this.getReadableDatabase();

        try{
            String sql = "INSERT INTO " + EVENT_REQUEST_TABLE_NAME +
                    " (user_id,owner_user ,event_id, status, user_name) VALUES(\'"
                    + userId + "\', \'" + ownerUser + "\', \'" + eventId + "\', \'" + status + "\', \'" + userName + "\')";
            db.execSQL(sql);
        } catch(Exception ex) {
            Log.e("error while insert config",ex+"");
        }
    }

    public void deleteUserRequest(String userId) {
        SQLiteDatabase db = this.getReadableDatabase();

        try{
            String sql = "DELETE FROM " + EVENT_REQUEST_TABLE_NAME +
                    " WHERE user_id = \'" + userId + "\' OR owner_user = \'" + userId + "\'";
            db.execSQL(sql);
        } catch(Exception ex) {
            Log.e("error while delete requests",ex+"");
        }
    }

    public Boolean checkIfConfigExists() {
        Boolean isExists = true;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM CONFIG";
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!(cursor.moveToFirst()) || cursor.getCount() == 0)
            {
                isExists = false;
            }
            db.close();
        }
        catch (Exception e){
            Log.e("error",e+"");
        }

        return isExists;
    }

    public Integer addEvent(Event event) {
        long rowInserted = 0;
        if (getEventById(event.Id).Id != null) {
            return updateEvent(event);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            try {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                ContentValues values = new ContentValues();

                values.put(KEY_ID, event.Id);
                values.put(KEY_NAME, event.Name);
                values.put(KEY_DESCRIPTION, event.Description);
                if (event.EventDate != null) {
                    values.put(KEY_DATE, df.format(event.EventDate));
                }
                values.put(KEY_EVENT_IMAGE, event.Image);
                values.put(KEY_CREATOR, event.CreatorName);
                values.put(KEY_CREATOR_ID, event.CreatorId);
                values.put(KEY_PLACE, event.Place);
                values.put(KEY_TIME, event.EventTime);
                rowInserted = db.insert(EVENT_TABLE_NAME, null, values);
                insertEventUsers(event.Id, event.AttendingUsers);
                db.close();

            } catch (Exception e) {
                Log.e("problem", e + "");
                throw e;
            }
        return Integer.valueOf(Long.toString(rowInserted));
        }
    }

    public Integer updateEvent(Event event) {
        long rowInserted = 0;
        if (event != null) {
            SQLiteDatabase db = this.getWritableDatabase();
            try {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

                ContentValues values = new ContentValues();
                values.put(KEY_NAME, event.Name);
                values.put(KEY_DESCRIPTION, event.Description);
                if(event.EventDate != null) {
                    values.put(KEY_DATE, df.format(event.EventDate));
                }
                values.put(KEY_EVENT_IMAGE, event.Image);
                values.put(KEY_CREATOR, event.CreatorName);
                values.put(KEY_CREATOR_ID, event.CreatorId);
                values.put(KEY_PLACE, event.Place);

                if(event.EventTime != null){
                    values.put(KEY_TIME, event.EventTime);
                }

                rowInserted = db.update(EVENT_TABLE_NAME,values, KEY_ID +"=\'"+event.Id +"\'" ,null);
                deleteEventUsers(event.Id);
                insertEventUsers(event.Id, event.AttendingUsers);
                db.close();

            } catch (Exception e) {
                Log.e("problem", e + "");
            }
        }

        return Integer.valueOf(Long.toString(rowInserted));
    }

    public void deleteEventUsers(String eventId) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Event> eventList = new ArrayList<Event>();

        try{
            String sql = "Delete from " + EVENT_ATTENDANT_TABLE_NAME + " where event_id = \'" + eventId + "\'";
            db.execSQL(sql);
        } catch(Exception ex) {
            Log.e("error while insert user events",ex+"");
        }
    }

    public void insertEventUsers(String eventId, List<User> users) {
        try{
            for (int i = 0; i < users.size(); i++) {
                if(users.get(i) != null) {
                    if (!checkIfUserexists(users.get(i).Id)) {
                        addUser(users.get(i));
                    }

                    String sql = "INSERT INTO " + EVENT_ATTENDANT_TABLE_NAME + " (event_id, user_id) VALUES(\'"
                            + eventId + "\', \'" + users.get(i).Id + "\')";
                    SQLiteDatabase db = this.getReadableDatabase();
                    db.execSQL(sql);
                    getEventAttendingUsers(eventId);
                    db.close();
                }
            }
        } catch(Exception ex) {
            Log.e("error while insert user events",ex+"");
        }
    }

    public List<User> getEventAttendingUsers(String eventId) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<User> userList = new ArrayList<User>();

        try{
            String QUERY = "SELECT * FROM "+ USER_TABLE_NAME + " WHERE _id IN " +
                    "(select user_id from " + EVENT_ATTENDANT_TABLE_NAME + " where event_id = \'" + eventId +"\')";
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    User user = new User();
                    user.Id = (cursor.getString(0));
                    user.Name = (cursor.getString(1));
                    user.Age = (parseInt(cursor.getString(2)));
                    user.Address = (cursor.getString(3));
                    user.AboutMe = (cursor.getString(4));
                    userList.add(user);
                }
            }
            db.close();
        }catch (Exception e){
            Log.e("error",e+"");
        }

        return userList;
    }

    public void getAllEventsAndObserve(EventListener listener) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Event> eventList = new ArrayList<Event>();

        try{
            String QUERY = "SELECT * FROM "+EVENT_TABLE_NAME;
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                while (cursor.moveToNext())
                {
                    Event event = BuildEvent(cursor);
                    event.AttendingUsers = getEventAttendingUsers(event.Id);
                    eventList.add(event);
                }
            }
            db.close();
        }catch (Exception e){
            Log.e("error",e+"");
        }
        listener.onAllEventsLoad(eventList);
    }

    private Event BuildEvent(Cursor cursor) {
        Event event = new Event();

        try {
            event.Id = (cursor.getString(0));
            event.Name = (cursor.getString(1));
            event.Description = (cursor.getString(2));
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            event.EventDate = (df.parse(cursor.getString(3)));

            event.CreatorName = (cursor.getString(5));
            event.CreatorId = (cursor.getString(6));
            event.Image = (cursor.getString(7));
            event.Place = (cursor.getString(8));
            event.EventTime = (cursor.getString(9));

        }
        catch (Exception e){
            Log.e("error",e+"");
        }

        return event;
    }


    public Event getEventById(String id) {
        Event event = null;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM "+ EVENT_TABLE_NAME + " WHERE " + KEY_ID +"='"+id+"'";
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                event = new Event();
                while (cursor.moveToNext())
                {
                     event = BuildEvent(cursor);
                     event.AttendingUsers = getEventAttendingUsers(event.Id);
                }
            }
            db.close();
        }
        catch (Exception e){
            Log.e("error",e+"");
        }
        return event;
    }

    public User getUserById(String id) {
        User user = null;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM "+ USER_TABLE_NAME + " WHERE " + KEY_ID +"=\'"+id+ "\'";
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!cursor.isLast())
            {
                user = new User();
                while (cursor.moveToNext())
                {
                    user.Id = (cursor.getString(0));
                    user.Name = (cursor.getString(1));
                    user.Age = (parseInt(cursor.getString(2)));
                    user.Address = (cursor.getString(3));
                    user.AboutMe = (cursor.getString(4));
                    user.ProfileImage = (ImageHelper.getImage(cursor.getBlob(5)));
                    user.MyRequests = this.getUserRequests(user.Id);
                    user.PendingRequest = this.getUserPendingRequests(user.Id);
                }
            }
            db.close();
        }
        catch (Exception e){
            Log.e("error",e+"");
        }
        return user;
    }

    public void deleteEventById(String id) {
        // delete all event attendents.
        deleteEventUsers(id);

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + EVENT_TABLE_NAME + " WHERE "+KEY_ID+"='"+id+"'");
        db.close();
    }

    public Integer updateUser(User user) {
        long rowInserted = 0;

        if(user != null)
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

            try{
                values.put(KEY_ID, user.Id);
                values.put(KEY_NAME, user.Name);
                values.put(KEY_ADDRESS, user.Address);
                values.put(KEY_AGE, user.Age);
                values.put(KEY_ABOUT, user.AboutMe);
                if(user.ProfileImage != null) {
                    values.put(KEY_IMAGE, ImageHelper.getBytes(user.ProfileImage));
                }
                rowInserted = db.update(USER_TABLE_NAME,values, KEY_ID +"="+user.Id,null);

                deleteUserRequest(user.Id);
                if(user.MyRequests != null) {
                    Object[] requests = user.MyRequests.values().toArray();
                    for(int i = 0; i < requests.length; i++) {
                        if(requests[i] != null){
                            insertEventRequest(((EventRequest)requests[i]).UserId, ((EventRequest)requests[i]).OwnerUserId,
                                    ((EventRequest)requests[i]).EventId, ((EventRequest)requests[i]).Status,
                                    ((EventRequest)requests[i]).UserName);
                        }
                    }
                }

                if(user.PendingRequest != null) {
                    Object[] pendingRequests = user.PendingRequest.values().toArray();
                    for(int j = 0; j < pendingRequests.length; j++) {
                        if(pendingRequests[j] != null) {
                            insertEventRequest(((EventRequest)pendingRequests[j]).UserId,
                                    ((EventRequest)pendingRequests[j]).OwnerUserId,
                                    ((EventRequest)pendingRequests[j]).EventId,
                                    ((EventRequest)pendingRequests[j]).Status,
                                    ((EventRequest)pendingRequests[j]).UserName);
                        }
                    }
                }

                db.close();

            }catch (Exception e){
                Log.e("problem",e+"");
                throw e;
            }
        }

        return Integer.valueOf(Long.toString(rowInserted));
    }

    public Integer addUser(User user) {
        if(checkIfUserexists(user.Id)){
            return updateUser(user);
        } else {
            return insertUser(user);
        }
    }

    public Integer insertUser(User user) {

        long rowInserted = 0;

        if(user != null)
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

            try{
                values.put(KEY_ID, user.Id);
                values.put(KEY_NAME, user.Name);
                values.put(KEY_ADDRESS, user.Address);
                values.put(KEY_AGE, user.Age);
                values.put(KEY_ABOUT, user.AboutMe);
                if(user.ProfileImage != null) {
                    values.put(KEY_IMAGE, ImageHelper.getBytes(user.ProfileImage));
                }

                rowInserted = db.insert(USER_TABLE_NAME, null, values);
                if(user.MyRequests != null) {
                    Object[] requests = user.MyRequests.values().toArray();
                    for(int i = 0; i < requests.length; i++) {
                        if(requests[i] != null){
                            insertEventRequest(((EventRequest)requests[i]).UserId, ((EventRequest)requests[i]).OwnerUserId,
                                    ((EventRequest)requests[i]).EventId, ((EventRequest)requests[i]).Status,
                                    ((EventRequest)requests[i]).UserName);
                        }
                    }
                }

                if(user.PendingRequest != null) {
                    Object[] pendingRequests = user.PendingRequest.values().toArray();
                    for(int j = 0; j < pendingRequests.length; j++) {
                        if(pendingRequests[j] != null) {
                            insertEventRequest(((EventRequest)pendingRequests[j]).UserId,
                                    ((EventRequest)pendingRequests[j]).OwnerUserId,
                                    ((EventRequest)pendingRequests[j]).EventId,
                                    ((EventRequest)pendingRequests[j]).Status,
                                    ((EventRequest)pendingRequests[j]).UserName);
                        }
                    }
                }
                if(eventListener != null) {
                    eventListener.setUser(user);
                }

                db.close();
            }catch (Exception e){
                Log.e("problem",e+"");
                throw e;
            }
        }

        return Integer.valueOf(Long.toString(rowInserted));
    }

    public Boolean checkIfUserexists(String userId) {

        Boolean isExists = true;

        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String QUERY = "SELECT * FROM "+ USER_TABLE_NAME +
                    " WHERE "+KEY_ID+"="+userId;
            Cursor cursor = db.rawQuery(QUERY, null);
            if(!(cursor.moveToFirst()) || cursor.getCount() == 0)
            {
                isExists = false;
            }
            db.close();
        }
        catch (Exception e){
            Log.e("error",e+"");
        }

        return isExists;
    }
}