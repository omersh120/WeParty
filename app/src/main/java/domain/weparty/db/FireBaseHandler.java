package domain.weparty.db;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.LinkedList;
import java.util.List;

import domain.weparty.Model.Event;
import domain.weparty.Model.EventRequest;
import domain.weparty.Model.User;
import domain.weparty.Utils.ImageHelper;
public class FireBaseHandler {

    static final String TAG = "FirebaseModel";
    public DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    DatabaseReference mEventsRef = mDatabase.child("events");
    DatabaseReference mUsersRef =  mDatabase.child("users");
    EventListener searchListener = null;
    public static EventDatabaseHandler mEventDatabaseHandler;
    private long _updatedTo;

    public FireBaseHandler(EventDatabaseHandler db, long lastUpdate) {
        _updatedTo = lastUpdate;
        mEventDatabaseHandler = db;
        try{

        }
        catch (Exception e)
        {
            Log.d(TAG, "error in firebase listener", e);
            throw e;
        }
    }

    public void getUserByIdAndObserve(final String userId, final EventListener listener) {
        mUsersRef.child(userId).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                            User user = dataSnapshot.getValue(User.class);
                            if (user != null) {
                                listener.setUser(user);
                            }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }

    public void setSearchListener(EventListener listener){
        this.searchListener = listener;
    }

    public void listenToNewEvents(final EventListener callback) {
        long now = System.currentTimeMillis();
        mEventsRef.orderByChild("Timestamp").startAt(now).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Event event = dataSnapshot.getValue(Event.class);
                mEventDatabaseHandler.addEvent(event);
                if(searchListener != null) {
                    searchListener.addEvent(event);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getAllEventsAndObserve(final EventListener callback, boolean isFirstTime) {
        long fromTime = 0;
        if(!isFirstTime){
            fromTime = _updatedTo;
        }

        mEventsRef.orderByChild("Timestamp").startAt(fromTime).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Event> list = new LinkedList<Event>();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    Event event = snap.getValue(Event.class);
                    mEventDatabaseHandler.addEvent(event);
                    list.add(event);
                }
                callback.onAllEventsLoad(list);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void addEvent(Event event){
        try {
            for (int i =0; i< event.AttendingUsers.size(); i++) {
                User curUser = (User) event.AttendingUsers.get(i);
                event.AttendingUsers.set(i, getUserDetails(curUser));
            }

            mEventsRef.child(event.Id).setValue(event);
            mEventsRef.child(event.Id).child("Timestamp").setValue(ServerValue.TIMESTAMP);
        }
        catch (Exception ex)
        {
            Log.d(TAG, "addEvent: error add to firebase", ex);
            throw ex;
        }
    }

    public void insertUser(User user) {
        if(!mUsersRef.child(user.Id).child("id").getKey().equals(user.Id)) {
            User tempUser = getUserDetails(user);
            mUsersRef.child(user.Id).setValue(tempUser);
        }
    }

    public User getUserDetails(User user) {
        return new User(user.Id, user.Name, user.Age, user.AboutMe, user.Address, null);
    }

    //add new user to db
    public void getAllUsers(){
        try {
            mUsersRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                        User newUser = dataSnapshot.getValue(User.class);
                        if (newUser != null) {
                            mEventDatabaseHandler.addUser(newUser);
                        }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
        catch (Exception ex)
        {
            Log.d(TAG, "addUser: error add to firebase", ex);
            throw ex;
        }
    }

    //add new user to db
    public void initialUser(final User user){
        try {
            mEventDatabaseHandler.addUser(user);
            mUsersRef.child(user.Id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists()) {
                        User newUser = dataSnapshot.getValue(User.class);
                        if (newUser != null) {
                            mEventDatabaseHandler.addUser(newUser);
                        }
                    } else {
                        insertUser(user);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
        catch (Exception ex)
        {
            Log.d(TAG, "addUser: error add to firebase", ex);
            throw ex;
        }
    }

    public void addEventRequest(final EventRequest request) {
        mUsersRef.child(request.UserId).child("MyRequests")
                .child(request.EventId + request.UserId).setValue(request);
    }

    public void updateEventRequest(final EventRequest request) {
        mUsersRef.child(request.UserId).child("MyRequests")
                .child(request.EventId + request.UserId).setValue(request);
    }


    public void updatePendingEventRequest(final EventRequest request) {
        mUsersRef.child(request.OwnerUserId).child("PendingRequest")
                .child(request.EventId + request.UserId).setValue(request);
    }

    public void updateUser(final User user) {
        mUsersRef.child(user.Id).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User newUser = dataSnapshot.getValue(User.class);
                        if (newUser != null) {
                            mUsersRef.child(newUser.Id).child("Address").setValue(user.Address);
                            mUsersRef.child(newUser.Id).child("AboutMe").setValue(user.AboutMe);
                            mUsersRef.child(newUser.Id).child("Age").setValue(user.Age);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }

    public void addEventPendingRequest(final EventRequest request) {
        mUsersRef.child(request.OwnerUserId).child("PendingRequest")
                .child(request.EventId + request.UserId).setValue(request);
    }

    // delete event from db
    public void deleteEvent(final String id) {
        mEventsRef.child(id).removeValue();
    }

    public static void saveImage(Bitmap imageBmp, String name, final ImageHelper.SaveImageListener listener){
        FirebaseStorage storage = FirebaseStorage.getInstance();

        StorageReference imagesRef = storage.getReference().child("images").child(name);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = imagesRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                listener.fail();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                listener.complete(downloadUrl.toString());
            }
        });
    }


    public static void getImage(String url, final ImageHelper.GetImageListener listener){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference httpsReference = storage.getReferenceFromUrl(url);
        final long ONE_MEGABYTE = 1024 * 1024;
        httpsReference.getBytes(3* ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap image = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
                listener.onSuccess(image);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception exception) {
                Log.d("TAG",exception.getMessage());
                listener.onFail();
            }
        });
    }
}
