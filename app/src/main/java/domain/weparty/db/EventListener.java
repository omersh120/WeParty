package domain.weparty.db;

import java.util.List;

import domain.weparty.Model.Event;
import domain.weparty.Model.User;


public interface EventListener {
    Integer addEvent(Event event);

    void setUser(User user);

    void onAllEventsLoad(List<Event> list);
}