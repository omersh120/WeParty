package domain.weparty;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import domain.weparty.Model.User;
import domain.weparty.Utils.ImageHelper;

import static java.lang.Integer.parseInt;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditUserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditUserFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private User user;
    ImageView profilePicImageView;
    TextView tvUserName;
    EditText edUserAddress;
    EditText edUserAbout;
    DiscreteSeekBar ageBar;

    private OnFragmentInteractionListener mListener;

    public EditUserFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = MainActivity.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_user, container, false);
        user = ((MyApplication)getActivity().getApplication())
                        .getUser(((MainActivity)getActivity()).getUser());
        ((MainActivity)getActivity()).setUser(user);
        setProfileDetails(view, user);
        saveButtonUserHandle(view);
        cancelButtonHandle(view);
        return view;
    }

    private void saveButtonUserHandle(final View container) {
        Button saveButton = (Button)container.findViewById(R.id.btnSaveEdit);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address= ((EditText)container.findViewById(R.id.userAddress)).getText().toString();
                String about= ((EditText)container.findViewById(R.id.userAbout)).getText().toString();
                int ageString =((DiscreteSeekBar) container.findViewById(R.id.edit_user_userAge)).getProgress();
                String age= Integer.toString(ageString);
                User newUser = validateUser(age, address, about);
                if(newUser != null) {
                    try{
                        ((MyApplication) getActivity().getApplication()).updateUser(newUser);
                        user = newUser;
                        ((MainActivity)getActivity()).setUser(user);
                        continueToProfileFragment();
                    }
                    catch (Exception e)
                    {
                        Log.d("Add User:", "failed to add user",e);
                        throw e;
                    }
                }
            }});
    }

    private User validateUser(String age, String address, String about) {
        try {
            return new User(user.Id,user.Name, parseInt(age),
                    about, address, user.ProfileImage);
        } catch(Exception ex) {
            Toast toast = Toast.makeText(getActivity(), "Oops! something wrong with your age", Toast.LENGTH_SHORT);
            toast.show();
            return null;
        }
    }

    private void cancelButtonHandle(final View container) {
        Button cancelButton = (Button)container.findViewById(R.id.btnCancelEdit);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueToProfileFragment();
            }});
    }

    private void continueToProfileFragment() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new UserProfileFragment().newInstance(null);
            fm.beginTransaction()
                    .replace(R.id.contentContainer, fragment)
                    .commit();
        }
    }

    private void setProfileDetails(View view, User user) {
        profilePicImageView = (ImageView) view.findViewById(R.id.profilePicture);
        tvUserName = (TextView) view.findViewById(R.id.userName);
        edUserAbout = (EditText) view.findViewById(R.id.userAbout);
        edUserAddress = (EditText) view.findViewById(R.id.userAddress);
        ageBar = (DiscreteSeekBar) view.findViewById(R.id.edit_user_userAge);

        tvUserName.setText(user.Name);
        edUserAbout.setText(user.AboutMe);
        edUserAddress.setText(user.Address);
        ageBar.setProgress(user.Age);
        profilePicImageView.setImageBitmap(ImageHelper.getRoundedCornerBitmap(getContext(),
                user.ProfileImage, 200, 200, 200, false, false, false, false));
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
