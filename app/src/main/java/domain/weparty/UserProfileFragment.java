package domain.weparty;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import dmax.dialog.SpotsDialog;
import domain.weparty.Model.Event;
import domain.weparty.Model.User;
import domain.weparty.Utils.ImageHelper;
import domain.weparty.db.EventListener;

import static domain.weparty.MainActivity.user;

public class UserProfileFragment extends Fragment {
    ImageView profilePicImageView;
    TextView tvUserName;
    TextView tvUserAbout;
    TextView tvUserAddress;
    TextView tvUserAge;
    String userId;
    private static final String ARG_ITEM = "user";
    boolean isUserLogon = false;


    public UserProfileFragment() {
        // Required empty public constructor
    }


    public static UserProfileFragment newInstance(String userId) {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        Bundle bundle = getArguments();
        User currUser;
        if (bundle == null ){
            setUser();
        }
        else {
            userId = getArguments().getString(ARG_ITEM);
            if (userId == null ){
                currUser = setUser();
                setProfileDetails(view, currUser);
            }
            else{
                final SpotsDialog dialog = new SpotsDialog(getActivity(), R.style.Custom);
                dialog.show();
                ((MyApplication)getActivity().getApplication()).getUserByIdAndOberve(userId, new EventListener() {
                    @Override
                    public Integer addEvent(Event event) {
                        return null;
                    }

                    @Override
                    public void setUser(User user) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        setProfileDetails(view, user);

                    }

                    @Override
                    public void onAllEventsLoad(List<Event> list) {

                    }
                });
            }
        }

        return view;
    }

    private User setUser() {
        user = ((MyApplication)getActivity().getApplication())
                .getUser(((MainActivity)getActivity()).getUser());
        ((MainActivity)getActivity()).setUser(user);
        isUserLogon = true;
        return user;
    }

    private void setProfileDetails(View view, User user) {

        profilePicImageView = (ImageView) view.findViewById(R.id.profilePicture);
        tvUserName = (TextView) view.findViewById(R.id.userName);
        tvUserAbout = (TextView) view.findViewById(R.id.userAbout);
        tvUserAddress = (TextView) view.findViewById(R.id.userAddress);
        tvUserAge = (TextView) view.findViewById(R.id.userAge);

        tvUserName.setText(user.Name);
        if(user.Age != 0) {
            tvUserAge.setText(String.valueOf(user.Age) + " years old");
        } else {
            tvUserAge.setText("What is your age?");
        }

        if(user.Address != null && user.Address != "") {
            tvUserAddress.setText("From " + user.Address);
        } else {
            tvUserAddress.setText("Where are you come from?");
        }

        if(user.AboutMe != null && user.AboutMe != "") {
            tvUserAbout.setText(user.AboutMe);
        } else {
            tvUserAbout.setText("Tell about yourself");
        }

        if (user.ProfileImage != null){
            profilePicImageView.setImageBitmap(
                    ImageHelper.getRoundedCornerBitmap(getContext(),
                            user.ProfileImage, 200, 200, 200, false, false, false, false));
        }
        else{
            profilePicImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isUserLogon) {
            inflater.inflate(R.menu.edit_menu, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Fragment fragment = fm.findFragmentById(R.id.fragment_container);

                if (fragment == null) {
                    fragment = new EditUserFragment();
                    fm.beginTransaction()
                            .replace(R.id.contentContainer, fragment)
                            .commit();
                }
                break;
            default:
                break;
        }
        return true;
    }

}
